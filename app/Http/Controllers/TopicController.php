<?php

namespace App\Http\Controllers;

use App\Topic;
use App\Transformers\TopicTransformer;
use Illuminate\Http\Request;

class TopicController extends Controller
{
    public function index() {
        $topics = Topic::latestFirst()->get();

        return fractal()
            ->collection($topics)
            ->transformWith(new TopicTransformer)
            ->toArray();
    }
}
